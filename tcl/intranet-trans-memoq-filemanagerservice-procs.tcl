# /packages/intranet-trans-memoq/tcl/intranet-trans-memoq-procs.tcl

ad_library {
	This interface has operations for file management (upload and download).
	
	Certain memoQ Server web service operations (such as IServerProjectService.ImportTranslationDocument for document import) require a file to be uploaded. IFileManagerInterface.BeginChunkedFileUpload and its related operations can be used to upload the file first, and then the file identifier returned by BeginChunkedFileUpload can be used to identify the uploaded file to be imported when calling IServerProjectServicedocument.ImportTranslationDocument.
	
	There are other MemoQ Server web service operations (such as IServerProjectServicedocument.ExportTranslationDocument for document export) that store the result on the MemoQ Server in a file. The file identifier returned by these operations can be used to download the file using IFileManagerInterface.BeginChunkedFileDownload and its related operations.
	
	Uploaded files and files created by web service operations (such as IServerProjectServicedocument.ExportTranslationDocument are no longer preserved than 24 hours (can be configured)on the MemoQ Server. Therefore, initiate processing operations (such as document import) immediately after file upload. It is also highly recommended to delete the uploaded file by calling DeleteFile if a file is no longer needed. For example after the processing (such as document import) has finished, and also after having downloaded the resulting file of a server operation (such as document export). This will preserve server resources.
	
	@author malte.sussdorff@cognovis.de
}

package require base64
namespace eval intranet_memoq::FileManagerService {}

ad_proc -public intranet_memoq::FileManagerService::BeginChunkedFileUpload {
	-fileName
	{-isZipped "false"}
} {
	Starts a new chunked file upload session.
	
	@param fileName Name or path of the file. This information is stored with the uploaded file. It can contain only characters that are valid on a Windows Operating System (e.g. '?', '\' are not allowed).
	@param isZipped If set to true, the file is assumed to be a zipped file, therefore it is unzipped on the server after upload.
	
	@return The session id (guid) of the newly started chunked file upload session. It should be provided as first parameter for the AddNextFileChunk and EndChunkedFileUpload. This guid is also the file identifier, that can be used by other web service operations after the file upload session has been ended by calling EndChunkedFileUpload.
	
} {
	set operation "BeginChunkedFileUpload"
	
	set m:BeginChunkedFileUpload(m:fileName) [encoding convertto "utf-8" $fileName]
	set m:BeginChunkedFileUpload(m:isZipped) $isZipped
	
 	intranet_chilkat::array_to_xml -xml_name tmXml -array_name "m:$operation"
	set response [im_trans_memoq_call -service_name FileManagerService -operation $operation -m_Xml $tmXml]
 	set responseXml [new_CkXml]
 	set success [CkXml_LoadXml $responseXml $response]
 	set success [CkXml_FindChild2 $responseXml "s:Body"]
 	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
 	set success [CkXml_FindChild2 $responseXml "${operation}Result"]
 	set session_id [CkXml_content $responseXml]
	delete_CkXml $responseXml
	
	return $session_id
}

ad_proc -public intranet_memoq::FileManagerService::AddNextFileChunk {
	-fileIdAndSessionId
	-fileData
} {
	Performs the upload of the next file chunk. The operation should be called in turns to upload the next chunk of the file. It is important that the interval between two AddNextFileChunk calls (and the interval between the call of BeginChunkedFileUpload and the first call of AddNextFileChunk) is less than a minute or two. If the interval is larger, then the upload session times out on the server and reserved resources (such as the file handle) are released, the upload can not continue.

	@param fileIdAndSessionId The session id (guid) of the chunked file upload session created by BeginChunkedFileUpload.
	@param fileData The bytes of the file chunk.
	
	@return 1 if successful
} {
	set operation "AddNextFileChunk"

	set m:AddNextFileChunk(m:fileIdAndSessionId) $fileIdAndSessionId
	set m:AddNextFileChunk(m:fileData) $fileData

 	intranet_chilkat::array_to_xml -xml_name tmXml -array_name "m:$operation"
	set response [im_trans_memoq_call -service_name FileManagerService -operation $operation -m_Xml $tmXml]
 	set responseXml [new_CkXml]
 	set success [CkXml_LoadXml $responseXml $response]

	# Might want to catch a failure?
 	set success [CkXml_FindChild2 $responseXml "s:Body"]
	if {[CkXml_FindChild2 $responseXml "s:Fault"]} {
		set success_p 0
	} else {
		set success_p 1
	}
	delete_CkXml $responseXml
	return $success_p
}


ad_proc -public intranet_memoq::FileManagerService::EndChunkedFileUpload {
	-fileIdAndSessionId
} {
	Ends the chunked file upload session. Call to indicate to the MemoQ Server that all chunks have been sent by calling AddNextFileChunk. It is very important to call this method as soon as possible after uploading the last chunk of data to release server resources (such as the file handle). If the uploaded file is zipped, then the unzipping is performed, and the function does not return until the unzipping of the file has finished.
	
	@param fileIdAndSessionId The session id (guid) of the chunked file upload session created by BeginChunkedFileUpload.
	
	@return 1 if successfull
} {
	set operation "EndChunkedFileUpload"

	set m:EndChunkedFileUpload(m:fileIdAndSessionId) $fileIdAndSessionId
	
 	intranet_chilkat::array_to_xml -xml_name tmXml -array_name "m:$operation"
	set response [im_trans_memoq_call -service_name FileManagerService -operation $operation -m_Xml $tmXml]
 	set responseXml [new_CkXml]

 	CkXml_LoadXml $responseXml $response

	CkXml_FindChild2 $responseXml "s:Body"
	set success [CkXml_FindChild2 $responseXml "s:Body"]
	if {[CkXml_FindChild2 $responseXml "s:Fault"]} {
		set success_p 0
	} else {
		set success_p 1
	}
	delete_CkXml $responseXml
	return $success_p
}

ad_proc -public intranet_memoq::FileManagerService::DeleteFile {
	-fileGuid
} {
	Deletes the file on the MemoQ Server. Call to delete uploaded and server generated files as soon as possible if they are no longer needed.

	@param fileGuid The identifier of the file to be deleted.
	
	@return 1 if successfull
} {
	set operation "DeleteFile"

	set m:DeleteFile(m:fileGuid) $fileGuid

 	intranet_chilkat::array_to_xml -xml_name tmXml -array_name "m:$operation"
	set response [im_trans_memoq_call -service_name FileManagerService -operation $operation -m_Xml $tmXml]
 	set responseXml [new_CkXml]

 	CkXml_LoadXml $responseXml $response

	CkXml_FindChild2 $responseXml "s:Body"
	set success [CkXml_FindChild2 $responseXml "s:Body"]
	if {[CkXml_FindChild2 $responseXml "s:Fault"]} {
		set success_p 0
	} else {
		set success_p 0
	}
	delete_CkXml $responseXml
	return $success_p
}
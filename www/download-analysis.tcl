ad_page_contract {
    Download Analyis
    
    @author malte.sussdorff@cognovis.de
    @creation-date 2017-07-20
    @cvs-id $Id$
} {
    project_id:integer,notnull
    {resultFormat "CSV_MemoQ"}
    {ShowResultsPerFile "true"}
    {IncludeLockedRows "false"}
    {DisableCrossFileRepetition "true"}
}

set project_nr [db_string project_nr "select project_nr from im_projects where project_id = :project_id"]

set serverProjectGuid [db_string project "select memoq_guid from im_projects where project_id = :project_id" -default ""]

if {$serverProjectGuid eq ""} { 
    set serverProjectGuid [im_trans_memoq_create_project -project_id $project_id]
}

if {$serverProjectGuid eq ""} { 
    set error_msg "MemoQ Project could not be created: You can't analyse for a project where we don't have a serverProjectGuid for" 
}

set operation "GetStatisticsOnProject"
    
set statsXml [new_CkXml]
CkXml_put_Tag $statsXml "m:$operation"
CkXml_NewChild $statsXml m:serverProjectGuid $serverProjectGuid

# ---------------------------------------------------------------
# Options
# ---------------------------------------------------------------
set childNode [CkXml_NewChild $statsXml "m:options" ""]
foreach option [list ShowResultsPerFile IncludeLockedRows DisableCrossFileRepetition] {
    CkXml_NewChild $childNode m:$option [set $option]
}
    
# Default options
if {$resultFormat eq "CSV_Trados"} {
    CkXml_NewChild $childNode m:Algorithm "Trados"
} else {
    CkXml_NewChild $childNode m:Algorithm "MemoQ"
}

foreach option [list Analysis_Homogenity Analysis_ProjectTMs Analyzis_DetailsByTM RepetitionPreferenceOver100 ShowCounts ShowCounts_IncludeTargetCount ShowCounts_IncludeWhitespacesInCharCount ShowCounts_StatusReport] {
    CkXml_NewChild $childNode m:$option "true"
}
    
CkXml_NewChild $statsXml m:resultFormat $resultFormat

CkXml_GetRoot2 $statsXml
    
set response [im_trans_memoq_call -service_name ServerProjectService -operation $operation -m_Xml $statsXml]
set responseXml [new_CkXml]
set success [CkXml_LoadXml $responseXml $response]
set success [CkXml_FindChild2 $responseXml "s:Body"]
set success [CkXml_FindChild2 $responseXml "${operation}Response"]
set success [CkXml_FindChild2 $responseXml "${operation}Result"]
set success [CkXml_FindChild2 $responseXml "ResultsForTargetLangs"]
set success [CkXml_FirstChild2 $responseXml]

ns_log Notice $response
set return_lol [list]
while {[expr $success == 1]} {
    # Go down one level
    set success [CkXml_FindChild2 $responseXml "ResultData"]
    
    set ck_string [new_CkString]
    CkString_append $ck_string "[CkXml_content $responseXml]"
    CkString_base64Decode $ck_string iso-8859-1
    set content	[CkString_getString $ck_string]

    delete_CkString $ck_string
    
    set success [CkXml_NextSibling2 $responseXml]
    set language "[CkXml_content $responseXml]"
    set status [CkXml_GetParent2 $responseXml]
    set success [CkXml_NextSibling2 $responseXml]
}
delete_CkXml $responseXml

set csv_file "[ns_tmpnam].csv"
set file [open $csv_file w]
fconfigure $file -encoding "utf-8"
puts $file "$content"
flush $file
close $file

set outputheaders [ns_conn outputheaders]
ns_set cput $outputheaders "Content-Disposition" "attachment; filename=${project_nr}_analysis.csv"
ns_returnfile 200 text/csv $csv_file

ad_page_contract {
    page to add a new translation resource
    
    @author malte.sussdorff@cognovis.de
    @creation-date 2017-07-20
    @cvs-id $Id$
} {
    company_id:integer,notnull
    return_url:optional
}

set user_id [ad_conn user_id]
set language_options [db_list_of_lists languages "select category,category_id from im_categories where category_type = 'Intranet Translation Language' and enabled_p = 't' order by sort_order, category"]
set company_name [im_name_from_id $company_id]



ad_form -name file-add -html { enctype multipart/form-data } -export { company_id return_url } -form {
    file_id:key
    {upload_file:file {label \#file-storage.Upload_a_file\#} {html "size 30"}}

	{source_language_id:text(select) {label "[lang::message::lookup {} intranet-translation.Source_Language {Source Language}]"} {options $language_options} }
	{target_language_id:text(select) {label "[lang::message::lookup {} intranet-translation.Target_Language {Target Language}]"} {options $language_options} }

    {subject_area_id:integer(im_category_tree),optional
    	{label "Subject Area"}
    	{custom {category_type "Intranet Translation Subject Area" translate_p 1}}
    }
	{name:text(text),optional {label "[lang::message::lookup {} intranet-translation.Name Name]"} {html {size 50}}}
	{description:text(textarea),optional {label Description} {html {cols 40 rows 5}}}
} -new_data {
       	
	set upload_tmpfile [template::util::file::get_property tmp_filename $upload_file]
	set upload_filename [template::util::file::get_property filename $upload_file]
	set file_extension [file extension $upload_filename]
	
	switch $file_extension {
		.tmx {
			# Find out if we already have a tmx for this combination
			if {$name eq ""} {
				set tmx_sql "select guid from im_trans_memoq_resources where type = 'tmx' and source_language_id = :source_language_id and target_language_id = :target_language_id and company_id = :company_id"
				if {$subject_area_id ne ""} {
					append tmx_sql " and subject_area_id = :subject_area_id"
				} else {
					append tmx_sql " and subject_area_id is null"
				}
			} else {
				set tmx_sql "select guid from im_trans_memoq_resouces where type = 'tmx' and name = :name"
			}
			set tm_guid [db_string tmx $tmx_sql -default ""]

			if {$tm_guid eq ""} {
				# Create Translation Memory
				set tm_info [intranet_memoq::TMService::CreateAndPublish \
					-source_language_id $source_language_id\
					-target_language_id $target_language_id\
					-company_id $company_id\
					-subject_area_id $subject_area_id\
					-name $name\
					-description $description]

				set tm_guid [lindex $tm_info 0]
				set name [lindex $tm_info 1]
				
				if {$tm_guid eq 0} {
					ad_return_error "Could not create TM" "There was an error creating the TM. PLease check the error log"
				} else {
					db_dml record_tmx "insert into im_trans_memoq_resources
						(guid,type,source_language_id,target_language_id,company_id,subject_area_id,name,description,filename,upload_date)
					values
						(:tm_guid,'tmx',:source_language_id,:target_language_id,:company_id,:subject_area_id,:name,:description,:upload_filename,now())
					"
				}
			}
			
			if {$tm_guid ne 0} {
				# Upload the file
				set segments [im_trans_memoq_import_tm_file -tmGuid $tm_guid -tmx_file $upload_tmpfile]
				db_dml update_tmx "update im_trans_memoq_resources set upload_date = now(), segments=:segments, filename = :upload_filename where guid = :tm_guid"
			}
		}
	}
} -after_submit {
	
	# Delete the uploaded file
	file delete -force $upload_tmpfile
   	if {[exists_and_not_null return_url]} {
	   	ad_returnredirect $return_url
    } else {
	    ad_returnredirect "/intranet/companies/view?company_id=$company_id"
    }
}

ad_return_template
